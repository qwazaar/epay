from os.path import join, dirname
from setuptools import setup, find_packages

import epay

setup(
    name=epay.__project__,
    version=epay.__version__,
    packages=find_packages(),
    long_description=open(join(dirname(__file__), 'README.md')).read(),
    include_package_data=True,
    test_suite='tests',
    author='El Barto',
    author_email=epay.__author__,
)